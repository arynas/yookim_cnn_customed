Requirements:

Code is written in Python (2.7) and requires Theano (0.7).

How to Operate:

GPU:
1. Run "THEANO_FLAGS=mode=FAST_RUN,device=gpu,floatX=float32 python conv_net_sentence.py -nonstatic -word2vec"

CPU: 
1. Run "THEANO_FLAGS=mode=FAST_RUN,device=cpu,floatX=float32 python conv_net_sentence.py -nonstatic -rand"

Original repo, visit:
https://github.com/yoonkim/CNN_sentence